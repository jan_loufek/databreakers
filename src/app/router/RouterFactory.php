<?php

namespace App;

use Nette\Application\IRouter;
use Nette\Application\Routers\RouteList;
use Nette\Application\Routers\Route;


class RouterFactory
{

	/**
	 * @return IRouter
	 */
	public static function createRouter() : IRouter
	{
		$router = new RouteList;
        $router[] = new Route('api/<action>[/<id>]',
            ['module' => 'Api', 'presenter' =>'Main', 'action' => 'default']);
        $router[] = new Route('<presenter>/<action>[/<id>]', 'Homepage:default');
		return $router;
	}


}
