<?php


namespace Chap\App\Models;


use Ublaboo\DataGrid\DataSource\IDataSource;
use Ublaboo\DataGrid\Utils\Sorting;


/**
 *  Model for DataGrid
 *
 * Class LogsDataModel
 * @package Chap\App\Models
 */
class LogsDataModel implements  IDataSource
{

    /** @var  ElasticConnector */
    private $elasticConnector;

    /** @var  int */
    private $limit;
    /** @var  int */
    private $offset;
    /** @var  array */
    private $filters;

    public function __construct(ElasticConnector $connector)
    {
        $this->elasticConnector = $connector;
    }

    /**
     * Get count of data
     * @return int
     */
    public function getCount() : int
    {
        $page = isset($this->filters['page_id']) ? $this->filters['page_id']->value : null;
        $profile = isset($this->filters['profile_id']) ? $this->filters['profile_id']->value : null;
        return $this->elasticConnector
            ->getErrorsByProfileAndPage($profile, $page, $this->limit, $this->offset)['total'];
    }

    /**
     * Get the data
     * @return array
     */
    public function getData() : array
    {
        $page = isset($this->filters['page_id']) ? $this->filters['page_id']->value : null;
        $profile = isset($this->filters['profile_id']) ? $this->filters['profile_id']->value : null;
        $result = $this->elasticConnector
            ->getErrorsByProfileAndPage($profile, $page, $this->limit, $this->offset)['hits'];
        // ID is needed by DataGrid
        foreach ($result as $k => $v) {
            $result[$k]['id'] = $k;
        }
        return $result;
    }

    /**
     * Filter data
     * @param array $filters
     * @return self
     */
    public function filter(array $filters) : self
    {
        $this->filters = $filters;
        return $this;
    }

    /**
     * Filter data - get one row
     * @param array $filter
     * @return self
     */
    public function filterOne(array $filter) : self
    {
        // TODO: Implement filterOne() method.
        return $this;
    }

    /**
     * Apply limit and offset on data
     * @param int $offset
     * @param int $limit
     * @return self
     */
    public function limit($offset, $limit) : self
    {
        $this->limit = $limit;
        $this->offset = $offset;
        return $this;
    }

    /**
     * Sort data
     * @param Sorting $sorting
     * @return self
     */
    public function sort(Sorting $sorting) : self
    {
        // TODO: Implement sort() method.
        return $this;
    }
}