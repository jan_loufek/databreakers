<?php

namespace Chap\App\Models;


use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use Nette\InvalidStateException;

class ElasticConnector
{
    /** @var Client*/
    private $client;

    /**
     * ElasticConnector constructor.
     * @param bool|string $host
     */
    public function __construct($host = false)
    {
        // specify host by constructor parameter or get by environment variables
        $connect = $host ?: getenv('ELASTICSEARCH_HOST'). ':' . getenv('ELASTICSEARCH_PORT');
        $this->client = ClientBuilder::create()
            ->setHosts([$connect])
            ->build();
    }

    /**
     * @param null|int $profileId
     * @param int $topN
     * @return array
     */
    public function getErrorsByProfile($profileId, $topN = 100) : array
    {
        return $this->getErrorsByProfileAndPage($profileId, null, $topN);
    }

    /**
     * @param null|int $pageId
     * @param int $topN
     * @return array
     */
    public function getErrorsByPage($pageId, $topN = 100) : array
    {
        return $this->getErrorsByProfileAndPage(null, $pageId, $topN);
    }


    /**
     * @param int|null$profileId
     * @param int|null$pageId
     * @param int $topN
     * @param int $offset
     * @return array
     */
    public function getErrorsByProfileAndPage($profileId, $pageId, $topN = 10, $offset = 0) : array
    {
        $query = [
            'action' => 'page_fetch',
            'page_id'  => $pageId,
            'profile_id'  => $profileId,
        ];
        return $this->retrieveData($query, $topN, $offset);
    }

    /**
     * Read occurrence for error codes
     *
     * @param array $params
     * @return int[] - error_code => occurrence
     * @throws InvalidStateException
     */
    private function readStats(array $params) : array
    {
        $params['size'] = 0;
        $params['body']['aggs'] = [
            'count' => [
                'terms' => [
                    'field' => 'error_code'
                ]
            ]
        ];
        $result_stats = $this->client->search($params);
        if (!isset($result_stats['aggregations']['count']['buckets'])) {
            throw new InvalidStateException('Wrong stats result');
        }
        $sums = [];
        foreach ($result_stats['aggregations']['count']['buckets'] as $sum) {
            $sums[$sum['key']] = $sum['doc_count'];
        }
        return $sums;
    }


    /**
     * Base method for process all "log requests"
     *
     * @param array $query
     * @param int $topN
     * @param int $offset
     * @return array
     */
    private function retrieveData(array $query, $topN, $offset) : array
    {
        $queries = [];
        //remove empty conditions by filter
        foreach (array_filter($query, function($v) {return !empty($v);}) as $key => $value) {
            $queries[] = ['match' => [$key => $value]];
        }
        // params for elastic request
        $params = [
            'index' => 'promoter-logs',
            'type'  => 'logs',
            'size'  => $topN,
            'from'  => $offset,
            'body'  => [
                'query' => [
                    'bool' => [
                        'must' => $queries,
                    ]
                ],
            ],
            'sort' => 'severity:desc',
        ];
        $stats = [];
        try {
            $stats = $this->readStats($params);
            /** @var $commonErrors  - Error code(s) of most occurred errors*/
            $commonErrors = array_keys($stats, max($stats));
            $params['body']['query']['bool']['filter'][] = ['terms' => ['error_code'=> $commonErrors ]] ;
            $result = $this->client->search($params);
        } catch (\Exception $e) {}

        // on error - no data
        if (!isset($result['hits'])) {
            return [
                'total' => 0,
                'max_score' => null,
                'hits'  => [],
                'sums'  => [],
                'status' => 'error',
            ];
        }
        $hits = $result['hits'];
        $hits['status'] = 'ok';
        $hits['sums'] = $stats;
        // pick only data
        foreach ($hits['hits'] as $key => $value) {
            $hits['hits'][$key] = $value['_source'];
        }
        return $hits;
    }

}