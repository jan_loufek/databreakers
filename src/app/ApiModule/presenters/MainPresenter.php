<?php

namespace App\ApiModule\Presenters;


use Chap\App\Models\ElasticConnector;
use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Presenter;

class MainPresenter extends Presenter
{

    /** @var  ElasticConnector @inject */
    public $elastic;


    /**
     * Empty - health request
     */
    public function actionDefault() : void
    {
        $this->sendResponse(new JsonResponse(['info' => 'Welcome']));
    }

    /**
     * Read Errors by page
     * @param int $id
     */
    public function actionPage(int $id) : void
    {
        $top = $this->getParameter('top', 10);
        $this->sendResponse(new JsonResponse($this->elastic->getErrorsByPage($id, $top)));
    }

    /**
     * Read Errors by profile
     * @param int $id
     */
    public function actionProfile(int $id) : void
    {
        $top = $this->getParameter('top', 10);
        $this->sendResponse(new JsonResponse($this->elastic->getErrorsByProfile($id, $top)));
    }


}
