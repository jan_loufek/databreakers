<?php

namespace App\Presenters;


use Chap\App\Controls\Logs\ILogsGridFactory;
use Chap\App\Controls\Logs\LogsGrid;
use Chap\App\Models\ElasticConnector;

class HomepagePresenter extends BasePresenter
{

    /** @var  ElasticConnector @inject */
    public $elastic;

    /**
     * @param ILogsGridFactory $factory
     * @return LogsGrid
     */
    public function createComponentLogGrid(ILogsGridFactory $factory) : LogsGrid
    {
        return $factory->create();
    }



}
