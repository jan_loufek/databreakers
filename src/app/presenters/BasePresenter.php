<?php

namespace App\Presenters;

use Kdyby\Autowired\AutowireComponentFactories;
use Kdyby\Autowired\AutowireProperties;
use Nette\Application\UI\Presenter;
use WebLoader\Nette\CssLoader;
use WebLoader\Nette\JavaScriptLoader;
use WebLoader\Nette\LoaderFactory;


abstract class BasePresenter extends Presenter
{
    use AutowireProperties;
    use AutowireComponentFactories;

    /**
     * @param LoaderFactory $f abstraktní továrna WebLoader
     * @return CssLoader
     */
    protected function createComponentCss(LoaderFactory $f) : CssLoader
    {
        return $f->createCssLoader('front');
    }

    /**
     * @param LoaderFactory $f abstraktní továrna WebLoader
     * @return JavaScriptLoader
     */
    protected function createComponentJs(LoaderFactory $f) : JavaScriptLoader
    {
        return $f->createJavaScriptLoader('front');
    }

}
