<?php
/**
 *
 */

namespace Chap\App\Controls\Logs;


interface ILogsGridFactory
{

    /**
     * @return LogsGrid
     */
    public function create() : LogsGrid;
}