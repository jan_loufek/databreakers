<?php

namespace Chap\App\Controls\Logs;


use Chap\App\Models\ElasticConnector;
use Chap\App\Models\LogsDataModel;
use Nette\Application\UI\Control;
use Ublaboo\DataGrid\DataGrid;

/**
 * Class ClientGrid
 * @package Chap\Ekon\AdminModule\Controls\Grids\Client
 */
class LogsGrid extends Control {

    /** @var  ElasticConnector */
    private $elasticConnector;

    /**
     * LogsGrid constructor.
     * @param ElasticConnector $connector
     */
    public function __construct(ElasticConnector $connector)
    {
        parent::__construct();
        $this->elasticConnector = $connector;
    }


    /**
     * Factory for DataGrid
     * @return DataGrid
     */
    public function createComponentGrid() : DataGrid
    {
        $grid = new DataGrid();
        $grid->addColumnText('hostname', 'Hostname');
        $grid->addColumnText('component', 'Komponenta');
        $grid->addColumnText('page_title', 'Titulek');
        $grid->addColumnText('service', 'Služba');
        $grid->addColumnText('error_type', 'Typ Chyby');
        $grid->addColumnText('error_message', 'Chyba');
        $grid->addColumnNumber('severity', 'Vážnost');
        $grid->addColumnNumber('page_id', 'ID stránky')->setFilterText();
        $grid->addColumnNumber('profile_id', 'ID profil')->setFilterText();
        $grid->setDataSource(new LogsDataModel($this->elasticConnector));
        return $grid;
    }

    public function render() : void
    {
        $this->getTemplate()->setFile(__DIR__ . '/template.latte');
        $this->getTemplate()->render();
    }
}
