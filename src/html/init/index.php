<?php

require("WCurl.php");

/**
 * @return string
 */
function generateHash() {
	$hash = hash('md5', rand());
	return $hash;
}

/**
 * @return array
 */
function generatePromotionLog() {
	$postId = rand(1, 1024);
	$profileId = rand(1, 1024);
	$log = [
		'action' => 'boosting_post',
		'post_id' => $postId,
		'post_title' => sprintf('Post #%d', $postId),
		'profile_id' => $postId,
		'profile_title' => sprintf('Profile #%d', $profileId),
		'retry' => rand(1, 16),
	];
	return $log;
}

/**
 * @return array
 */
function generatePerformanceLog() {
	$log = [
		'action' => 'fetch_fb_ad',
		'duration' => rand(16, 512) / 1000,
	];
	return $log;
}

/**
 * @return array
 */
function generateScraperLog() {
	$scraperItemId = rand(1, 1024);
	$id = rand(1, 1024);
	$types = [
		'ADS',
		'AD_SETS',
		'AD_INSIGHTS',
	];
	$log = [
		'action' => 'scraper_item_process',
		'scraper_item_id' => $scraperItemId,
		'type' => $types[rand(0, 2)],
		'object_id' => $id,
		'rate_limit_parent_id' => rand(1, 1024),
		'retry' => rand(1, 16),
		'duration' => rand(16, 512) / 1000,
	];
	return $log;
}

/**
 * @return array
 */
function generateFBPageError() {
	$pageId = rand(1, 1024);
	$profileId = rand(1, 1024);
	$errorId = rand(1, 4);
	$log = [
		'component' => 'facebook_service',
		'action' => 'page_fetch',
		'fb_api' => 'graph',
		'endpoint' => 'page',
		'page_id' => $pageId,
		'page_title' => sprintf('Page #%d', $pageId),
		'profile_id' => $profileId,
		'profile_title' => sprintf('Profile #%d', $profileId),
		'message' => 'Error while fetching page',
		'error_type' => 'Facebook',
		'error_code' => $errorId,
		'error_message' => sprintf('Error #%d', $errorId),
	];
	return $log;
}

/**
 * @param string $index
 * @param string $hash
 * @param array $data
 * @return bool
 */
function toElasticSearch($index, $hash, array $data) {
	$logBody = [
		'@timestamp' => (new DateTime())->getTimestamp(),
		'@version' => 1,
		'msg_type' => 'log',
		'hostname' => 'promoter.databreakers.com',
		'service' => 'promoter-api',
		'environment' => 'dev',
		'severity' => 7,
	];
	$log = array_merge($logBody, $data);
	if(getenv('ELASTICSEARCH_HOST') === false || getenv('ELASTICSEARCH_PORT') === false) {
		echo sprintf('Environment variable ELASTICSEARCH_HOST or ELASTICSEARCH_PORT not defined.') . "\n";
		return false;
	}
	$ELASTIC_HOST = getenv('ELASTICSEARCH_HOST');
	$ELASTIC_PORT = getenv('ELASTICSEARCH_PORT');
	WCurl::request('POST', sprintf('http://%s:%s/%s/logs/%s', $ELASTIC_HOST, $ELASTIC_PORT, $index, $hash), '', $log);
	return true;
}

/**
 * @return void
 */
function initElasticSearch() {
	$numLogs = 1000;
	$index = 'promoter-logs';
	foreach(range(0, $numLogs-1) as $i) {
		toElasticSearch($index, generateHash(), generatePromotionLog());
	}
	foreach(range(0, $numLogs-1) as $i) {
		toElasticSearch($index, generateHash(), generatePerformanceLog());
	}
	foreach(range(0, $numLogs-1) as $i) {
		toElasticSearch($index, generateHash(), generateScraperLog());
	}
	foreach(range(0, $numLogs-1) as $i) {
		toElasticSearch($index, generateHash(), generateFBPageError());
	}
}

initElasticSearch();
