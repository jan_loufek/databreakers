# Řešení úlohy
 úloha je řešena jako kompozice tří docker kontainerú

 Spuštění pomocí `docker-compose up -d`

 Aplikace běží na portu 8100 (http protokol) - volba portu je "náhodná" aby neblokovala jiné služy. 
 Případně lze snadně upravit v docker--compose.yml
 
 Aplikace má dva pohledy
 1) klasický frontend `http://host:8100`
 2) API  `http://host:8100/api` 
    - endpoint list podle profilu `http://host:8100/api/profile/[id]`
    - endpoint list podle stránky `http://host:8100/api/page/[id]`
    - oba dotazy vrací top 10 výsledků, které lze ovlivnit GET parametrem top
     
 Hlavní třída pro komunikaci s ES je `ElasticConnector`, ve které je jedna zobecněná metoda `retrieveData` pro komunikaci a tu využívají další specifikované metody.


# Elasticsearch úloha - zadání

## Cíl

Získání informace o počtu chyb a výpis top *n* nejvíce se vyskytujících chyb z Elasticsearche.
Filtrování chyb 1. profilem uživatele 2. Facebook stránkou.

## Výstup

Implementace php komponenty s třídou, která bude obsahovat dvě metody:  
`function getErrorsByProfile($profileId, $topN);`  
`function getErrorsByPage($pageId, $topN);`  
které vracejí objekt s číselným atributem o celkovém počtu chyb a atribut s polem (o velikosti *n*)
chybových hlášek + počet výskytů jednotlivých chybových hlášek.  

Konkrétní podoba výstupní struktury není přesně určená. Ideálně by měla být navržená tak, aby byla
rovnou použitelná v API pro webový frontend.

"Bonusově" může mít řešení jednoduché REST API, které komponentu zavolá a vypíše výstup.

## Data

Objekty v ElasticSearchi jsou fake logy ze systému. Část logů jsou logy chyb, které vždy obsahují
atribut `error_message`, dále pak `profile_id` a `page_id`,
kde jsou uložené číselné id pro profil a stránku, a zajímají nás pouze logy s atributem
`action=page_fetch`.

## ElasticSearch

Spouští se lokálně přes docker `docker run --it -p 9200:9200 elasticsearch:5.2`, na portu 9200
je následně dostupný běžící elasticsearch. (tip: Docker zřejmě pro běh potřebuje o něco víc než 1 GB
paměti.)

Tento projekt obsahuje soubor init_elasticsearch.php, který nahraje fake logy – generuje náhodná
id atd. Skript vyžaduje proměnné prostředí `ELASTICSEARCH_HOST` a `ELASTICSEARCH_PORT`, které
v základu budou `localhost` a `9200`. Id profilu resp. stránky se nagenerují v rozsahu 1-1024.
